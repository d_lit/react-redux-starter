import React from 'react';
import { renderRoutes } from 'app/components/RouteWithSubRoutes';

const CoreLayout = ({ routes }) => (
  <div>
    {renderRoutes(routes)}
  </div>
);

export default CoreLayout;
