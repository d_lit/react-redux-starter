import registerStore, {
  registerReducer,
  registerMiddleware,
  registerEnhancer,
} from './store/registry';

import registerRoutes from './routes/registry';

const registerModule = (config) => {
  switch (typeof config) {
    case 'function': {
      const api = {
        store: {
          registerReducer,
          registerMiddleware,
          registerEnhancer,
        },
        routes: {
          registerRoutes,
        },
      };

      return config(api);
    }

    case 'object': {
      const api = {
        store: registerStore,
        routes: registerRoutes,
      };

      return Object.keys(config).reduce((result, key) => {
        if (!api[key]) {
          return result;
        }

        if (typeof config[key] === 'function') {
          return {
            ...result,
            [key]: config[key](api[key]),
          };
        }

        return {
          ...result,
          [key]: api[key](config[key]),
        };
      }, {});
    }

    default:
      return null;
  }
};

const registerModules = modules => modules.map(mod => registerModule(mod));

export default registerModules;

export {
  registerModule,
};
