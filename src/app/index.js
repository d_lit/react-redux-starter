import app from './app';

const init = (mods = []) => app.init(mods);

export default init;
