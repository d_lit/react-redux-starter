import React from 'react';
import {
  routerReducer,
  routerMiddleware,
} from 'react-router-redux';

import { history } from 'app/routes';

export default {
  store: {
    reducers: {
      router: routerReducer,
    },
    middlewares: {
      router: routerMiddleware(history),
    },
  },
  routes: [{
    path: '/',
    component: () => (<h1>App ready!</h1>),
  }],
};

