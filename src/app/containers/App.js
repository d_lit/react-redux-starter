import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { history } from 'app/routes';

import CoreLayout from 'app/layouts/CoreLayout';

// eslint-disable-next-line react/prefer-stateless-function
class App extends Component {
  render() {
    const { store, routes } = this.props;
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <CoreLayout routes={routes} />
        </ConnectedRouter>
      </Provider>
    );
  }
}

export default App;
