import { createStore } from 'redux';

import createReducer from './createReducer';
import createEnhancer from './createEnhancer';

const initStore = (initialState) => {
  const reducer = createReducer();
  const enhancer = createEnhancer();

  return createStore(reducer, initialState, enhancer);
};

export default initStore;

export {
  createReducer,
  createEnhancer,
};
