import { combineReducers } from 'redux';

import { reducersRegistry } from '../registry/reducers';

export default () => {
  if (reducersRegistry.size) {
    return combineReducers(Array.from(reducersRegistry).reduce(
      (reducers, [name, reducer]) => ({
        ...reducers,
        [name]: reducer,
      }), {},
    ));
  }

  return state => ({
    ...state,
    app: {
      mounted: true,
    },
  });
};
