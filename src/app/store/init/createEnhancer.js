import { compose } from 'redux';

import createMiddleware from './createMiddleware';

import { enhancersRegistry } from '../registry/enhancers';

const createEnhancer = () => {
  let composeEnhancers = compose;

  /* eslint-disable no-underscore-dangle, no-undef */
  if (process.env.NODE_ENV === 'development') {
    const composeWithDevToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

    if (typeof composeWithDevToolsExtension === 'function') {
      composeEnhancers = composeWithDevToolsExtension;
    }
  }
  /* eslint-enable no-underscore-dangle, no-undef */

  return composeEnhancers(
    createMiddleware(),
    ...Array.from(enhancersRegistry).reduce(
      (result, [name, value]) => ({
        ...result,
        [name]: value,
      }), [],
    ),
  );
};

export default createEnhancer;
