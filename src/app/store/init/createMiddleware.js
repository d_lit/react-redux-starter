import { applyMiddleware } from 'redux';

import { middlewaresRegistry } from '../registry/middlewares';

export default () => applyMiddleware(
  ...Array.from(middlewaresRegistry).reduce(
    (output, [name, middleware]) => ({
      ...output,
      [name]: middleware,
    }),
    [],
  ),
);
