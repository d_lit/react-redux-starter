const enhancersRegistry = new Map();

const registerEnhancer = (name, enhancer) => (
  name && enhancer && !enhancersRegistry.has(name)
    ? enhancersRegistry.set(name, enhancer)
    : null
);

export default registerEnhancer;

export {
  enhancersRegistry,
};
