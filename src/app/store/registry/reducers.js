// import { createReducer } from '../util/createReducer';

const reducersRegistry = new Map();

const registerReducer = (key, reducer) => (
  key && reducer && !reducersRegistry.has(key)
    ? reducersRegistry.set(key, reducer)
    : null
);

// const injectReducer = (store, { key, reducer }) => (
//   registerReducer(key, reducer)
//     ? store.restoreReducer(createReducer())
//     : null
// );

export default registerReducer;

export {
  reducersRegistry,
  // injectReducer,
};
