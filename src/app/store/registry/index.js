import registerReducer from './reducers';
import registerMiddleware from './middlewares';
import registerEnhancer from './enhancers';

const registerStore = (config) => {
  switch (typeof config) {
    case 'function': {
      const api = {
        registerReducer,
        registerMiddleware,
        registerEnhancer,
      };

      return config(api);
    }

    case 'object': {
      const api = {
        reducers: registerReducer,
        middlewares: registerMiddleware,
        enhancers: registerEnhancer,
      };

      return Object.keys(config).reduce((result, key) => {
        if (!api[key]) return result;

        switch (typeof config[key]) {
          case 'function':
            return { ...result, [key]: config[key](api[key]) };
          case 'object':
            return { ...result, [key]: api[key](config[key]) };
          default:
            return result;
        }
      }, {});
    }

    default:
      return null;
  }
};

export default registerStore;

export {
  registerReducer,
  registerMiddleware,
  registerEnhancer,
};
