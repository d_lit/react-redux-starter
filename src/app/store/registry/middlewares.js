const middlewaresRegistry = new Map();

const registerMiddleware = (name, middleware) => (
  name && middleware && !middlewaresRegistry.has(name)
    ? middlewaresRegistry.set(name, middleware)
    : null
);

export default registerMiddleware;

export {
  middlewaresRegistry,
};

