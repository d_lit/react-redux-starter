// React
import React from 'react';
import ReactDOM from 'react-dom';
// _.* Magic *._
import registerModules from './modules';
import initStore from './store';
import initRoutes from './routes';
// Core
import core from './core';
// Service Worker
import registerSW from './util/registerSW';
// Bundle
import App from './containers/App';

const app = {
  init(modules = []) {
    registerModules([core, ...modules]);
    return this;
  },
  render(
    mountNode = 'root',
    initialState = {
      app: {
        mounted: false,
      },
    },
  ) {
    const store = initStore(initialState);
    const routes = initRoutes(store);

    ReactDOM.render(
      <App store={store} routes={routes} />,
      document.getElementById(mountNode),
    );

    registerSW();
  },
};

export default app;
