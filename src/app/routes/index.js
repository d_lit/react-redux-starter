import registerRoutes, { routesRegistry } from './registry';
import history from './history';

const initRoutes = store => ([
  ...Array.from(routesRegistry).reduce(
    (routes, route) => ([
      ...routes,
      typeof route === 'function'
        ? route(store)
        : route,
    ]), [],
  ),
]);

export default initRoutes;

export {
  registerRoutes,
  history,
};
