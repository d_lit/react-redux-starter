const routesRegistry = new Set();

const registerRoute = route => (
  route && !routesRegistry.has(route)
    ? routesRegistry.add(route)
    : null
);

const registerRoutes = routes => (
  Array.isArray(routes)
    ? routes.map(route => registerRoute(route))
    : null
);

export default registerRoutes;

export {
  routesRegistry,
};
