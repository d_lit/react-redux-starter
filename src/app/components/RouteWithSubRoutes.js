import React from 'react';
import { Route } from 'react-router-dom';
import uniqueId from 'lodash/uniqueId';

const renderComponent = route => props => (
  <route.component {...props} routes={route.routes} />
);

const RouteWithSubRoutes = route => (
  <Route path={route.path} render={renderComponent(route)} />
);

const renderRoutes = routes => routes.map(route => (
  <RouteWithSubRoutes key={uniqueId()} {...route} />
));

export default RouteWithSubRoutes;
export {
  renderRoutes,
};
