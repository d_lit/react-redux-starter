import { Component } from 'react';

class Bundle extends Component {
  state = {
    mod: null,
  }

  componentWillMount() {
    this.load(this.props);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.load !== this.props.load) {
      this.load(nextProps);
    }
  }

  update(mod) {
    this.setState({
      mod: mod.default ? mod.default : mod,
    });
  }

  load(props) {
    this.setState({
      mod: null,
    });
    if (props.load) {
      props.load(this.update.bind(this));
    }
  }

  render() {
    return this.state.mod
      ? this.props.children(this.state.mod)
      : null;
  }
}

export default Bundle;
