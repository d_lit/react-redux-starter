// _.* Magic *._
import init from './app';
// Modules
import modules from './modules';
// Styles
import './index.scss';

(async (mods) => {
  try {
    const app = await init(mods);
    app.render();
  } catch (error) {
    throw (error);
  }
})([]);
